import React, { Component } from 'react';
import './App.css';
import { HomePage } from "./containers/HomePage"
import  NewPostPage  from "./containers/NewPostPage"
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Provider } from 'react-redux'
import rootReducer from './reducers'
import thunkMiddleware from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux';
import Thread from './containers/Thread';
import { Router, Route, Switch } from 'react-router-dom'
import helpers from "./helpers"

const store = createStore(
  rootReducer,
  applyMiddleware(
    thunkMiddleware,
  )
);
class App extends Component {
  render() {
    return (
      <Router history={helpers.history}>
        <MuiThemeProvider>
          <Provider store={store}>
            <Switch>
              <Route exact  path="/" component={HomePage} />
              <Route path="/thread" component={Thread} />
              <Route path="/newPost" component={NewPostPage} />
            </Switch>


          </Provider>
        </MuiThemeProvider>
      </Router>

    );
  }
}

export default App;
