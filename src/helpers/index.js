import { createHashHistory } from 'history'

const history = createHashHistory()
const baseURL = "http://127.0.0.1:8000"
const pageSize = 10
const owner_id = 1

export default global={
    history,
    baseURL,
    owner_id,
    pageSize,
}