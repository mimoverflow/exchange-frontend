import React from "react"
import { RaisedButton } from 'material-ui';
import Preview from "./Preview"
import { PreviewListActions } from '../actions/PreviewListActions'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

class PreviewList extends React.Component {
    componentWillMount() {
        this.props.fetchPreviewList();
    }

    render() {
        let fetchedList = this.props.previewList.results;

        let previews = [];
        previews.push(<br key={-fetchedList.length} />);
        for (let i = 0; i < fetchedList.length; i++) {
            previews.push(<Preview key={fetchedList[i].id} content={fetchedList[i].content}
                title={fetchedList[i].title} id={fetchedList[i].id} user="Test User" />);
            previews.push(<br key={-i} />);
        }

        previews.push(
            <RaisedButton fullWidth={true} backgroundColor="#00A0FF" labelColor="#ffffff" key={-fetchedList.length - 1}
                onClick={() => this.props.fetchNextPreview(this.props.previewList.next)}>
                More threads
            </RaisedButton>);

        return (<div>{previews}</div>);
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        previewList: state.previewList
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchPreviewList: () => {
            dispatch(PreviewListActions.fetchPreviewList());
        },
        fetchNextPreview: (url) => {
            dispatch(PreviewListActions.fetchNextPreview(url));
        }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PreviewList));
