import React from "react"
import { Card, CardHeader, CardText } from 'material-ui/Card';
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { ThreadActions } from '../actions'

class ReAnswer extends React.Component {
    render() {
        return (
            <div>
                <Card>
                    <CardHeader
                    />
                    <CardText>
                    <pre>
                        {this.props.content}
                    </pre>
                    </CardText>
                </Card>
            </div>

        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        currentThread: state.currentThread
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchThread: (id) => {
            dispatch(ThreadActions.fetchThread(id));
        }
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ReAnswer));