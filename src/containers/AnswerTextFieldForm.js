import React from 'react';
import TextField from 'material-ui/TextField';
import { RaisedButton } from 'material-ui';
import { ThreadActions } from '../actions/ThreadActions'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

class AnswerTextFieldForm extends React.Component {
  constructor() {
    super()
    this.state = {
      content: '',
    }
  }  
  handleContentChange(ev) {
    this.setState({
      content: ev.target.value
    })
  }
  buttonHandler(ev) {
      ev.preventDefault();
      this.props.fetchNewAnswer(this.props.thread_id, this.state.content);
      let cur_thread_id=localStorage.getItem("cur_thread_id");
      this.props.fetchThread(cur_thread_id);
      this.setState({ content : ""});
  }  

  render() {
    return (
      <div>
        <TextField
          hintText="Answer"
          fullWidth={true}
          multiLine={true}
          rows={1}
          value={this.state.content}
          onChange={this.handleContentChange.bind(this)}
        /><br />
        <br />
        <RaisedButton fullWidth={true} backgroundColor="#00A0FF" labelColor="#ffffff"
        onClick={this.buttonHandler.bind(this)}>
          Add Answer
        </RaisedButton>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
    return {
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchNewAnswer: (id, content) => {
            dispatch(ThreadActions.fetchNewAnswer(id, content));
        },
         fetchThread: (id) => {
            dispatch(ThreadActions.fetchThread(id));
        }
   
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AnswerTextFieldForm));