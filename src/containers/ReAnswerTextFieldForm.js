import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { ThreadActions } from '../actions/ThreadActions'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

class ReAnswerTextFieldForm extends React.Component {
  constructor() {
    super()
    this.state = {
      content: '',
    }
  }  
  handleContentChange(ev) {
    this.setState({
      content: ev.target.value
    })
  }
  buttonHandler(ev) {
      ev.preventDefault();
      this.props.fetchNewReAnswer(this.props.answer_id, this.state.content);
      let cur_thread_id=localStorage.getItem("cur_thread_id");
      this.props.fetchThread(cur_thread_id);
      this.setState({ content : ""});
        }  
  render() {
    return (
      <div>
        <TextField
          hintText="Content"
          fullWidth={true}
          multiLine={true}
          rows={1}
          value={this.state.content}
          onChange={this.handleContentChange.bind(this)}
        /><br />
        <br />
        <RaisedButton fullWidth={true} backgroundColor="#00A0FF" labelColor="#ffffff"
        onClick={this.buttonHandler.bind(this)}>
          Add Answer
        </RaisedButton>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
    return {
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchNewReAnswer: (id, content) => {
            dispatch(ThreadActions.fetchNewReAnswer(id, content));
        },
         fetchThread: (id) => {
            dispatch(ThreadActions.fetchThread(id));
        }
   
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ReAnswerTextFieldForm));