import React from "react"
import { Card, CardHeader, CardText } from 'material-ui/Card';
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { ThreadActions } from '../actions'
import ReAnswer from './ReAnswer'
import ReAnswerTextFieldForm from "./ReAnswerTextFieldForm"

class Answer extends React.Component {
    render() {
        let rendered_reanswers = []
        const reanswers = this.props.reanswers;
        rendered_reanswers.push(<br key={-1} />);

        for (let i = 0; i < reanswers.length; i++) {
            rendered_reanswers.push(<ReAnswer key={i} content={reanswers[i].content} user={reanswers[i].owner} />);
            rendered_reanswers.push(<br key={i + reanswers.length} />);
        }

        return (
            <div>
                <Card>
                    <CardHeader
                    />
                    <CardText>
                    <pre>
                        {this.props.content}
                    </pre>
                    </CardText>
                </Card>
                <ReAnswerTextFieldForm answer_id={this.props.id}/>
                <br /> 
                <div className="container">
                    <div className="col-md-3">
                    </div>
                    <div className="col-md-12">
                        {rendered_reanswers}
                    </div>
                </div>
            </div>

        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        currentThread: state.currentThread
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchThread: (id) => {
            dispatch(ThreadActions.fetchThread(id));
        }
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Answer));