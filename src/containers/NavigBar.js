import "../../node_modules/bootstrap/dist/css/bootstrap.min.css"
import { Input } from 'reactstrap';
import {connect} from 'react-redux';
import React from 'react';
import {ThreadActions} from '../actions'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';

class Search extends React.Component{
  searchHandler(ev){
     if(ev.charCode === 13){
      ev.preventDefault();
            this.props.search(ev.target.value); 
    }
  }

  render(){
    return(<div> 
        <Input placeholder="Search..." onKeyPress={this.searchHandler.bind(this)}/>      
      </div>)
  }
}
const mapStateToProps = (state, ownProps) => {
    return {}
}

const mapDispatchToProps = (dispatch, ownProps) => {
     return {
        search: (keyword) => {
            dispatch(ThreadActions.searchThreads(keyword));
        }
   
    }
}
const SearchBar= connect (mapStateToProps,mapDispatchToProps)(Search)

export class NavigBar extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  search(){

  }
  render() {
    return (
      <div>
        <Navbar style={{ backgroundColor: '#00A0FF', color: '#000000'}} light expand="md">
          <NavbarBrand href="/" style={{ color: '#ffffff'}}>MIM Overflow</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
              </NavItem>
              <SearchBar/>

              <NavItem>
                <NavLink href="/#/newPost/">New post</NavLink>
              </NavItem>
              <UncontrolledDropdown nav inNavbar >
              </UncontrolledDropdown>
              <UncontrolledDropdown nav inNavbar >
                <DropdownToggle nav caret>
                  Log in
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    CAS
                  </DropdownItem>
                  <DropdownItem>
                    E-mail
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
              <NavItem>
                <NavLink href="/components/">Sign up</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}



