import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { Card, CardHeader,   CardText } from 'material-ui/Card';
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css"
import { NavigBar } from './NavigBar'
import Answer from './Answer'
import AnswerTextFieldForm from "./AnswerTextFieldForm"
import { ThreadActions } from '../actions'


class Thread extends React.Component {
    componentWillMount(){
        let thread_id = window.localStorage.getItem("cur_thread_id")
        this.props.fetchThread(thread_id)
    }
    render() {
        let renderedAnswers = []
        let answers=this.props.currentThread.answers;
        let id = this.props.currentThread.id
        renderedAnswers.push(<br key={-1}/>);
        
        for(let i=0;i<answers.length;i++){
            renderedAnswers.push(<Answer reanswers={answers[i].reanswers} key={i} content={answers[i].content} 
                                    user={answers[i].owner} id={answers[i].id}  />);
            renderedAnswers.push(<br key={i+answers.length}/>);
        }

        return (
        <div>
            <NavigBar />
            <br></br>
            <div className="container">
                <div className="col-md-3"> </div>
                <div className="col-md-9">
                    <Card >
                        <CardHeader
                            title={this.props.currentThread.title}
                            subtitle={this.props.currentThread.user}
                        />
                        <CardText>
                        <pre>
                            {this.props.currentThread.content}
                            </pre>  
                        </CardText> 
                </Card>
                <AnswerTextFieldForm thread_id={id}/>
                </div>
                <div className="container">
                    <div className="col-md-6"> </div>
                    <div className="col-md-9">
                        {renderedAnswers}
                    </div>
                </div>
            </div>
        </div>);
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        currentThread: state.currentThread
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchThread: (id) => {
            dispatch(ThreadActions.fetchThread(id));
        }
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Thread));