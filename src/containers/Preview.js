import React from "react"
import { Card, CardActions, CardHeader, CardText } from 'material-ui/Card';
import {  FlatButton } from 'material-ui';
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { ThreadActions } from '../actions'


class Preview extends React.Component {
    buttonHandler(ev) {
        ev.preventDefault();
        this.props.fetchThread(this.props.id);
    }

    render() {
        return (
            <div>
                <Card>
                    <CardHeader
                        title={this.props.title}
                        actAsExpander={true}
                        showExpandableButton={true}
                    />
                    <CardActions>
                        <FlatButton label="See the post" onClick={this.buttonHandler.bind(this)} />
                    </CardActions>
                    <CardText expandable={true}>
                        <pre>
                        {this.props.content}
                        </pre>
                    </CardText>
                </Card>
            </div>

        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchThread: (id) => {
            dispatch(ThreadActions.fetchThread(id));
        }
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Preview));