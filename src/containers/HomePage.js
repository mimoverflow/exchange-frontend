import React, { Component } from 'react'
import PreviewList  from "./PreviewList"
import { NavigBar} from "./NavigBar"
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css"

export class HomePage extends Component {
    render() {
        return (
            <div style={{ backgroundColor: '#ffffff', color: '#000000'}}>
                <NavigBar />
                
                <div className="container">
                    <br></br>
                    <span>Top Threads:</span>
                    <div className="col-md-3">
                    </div>
                    <div className="col-md-9">
                        <PreviewList />
                    </div>
                </div>
            </div >
        );
    }
}


