import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

export class SubmitButton extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div>
        <RaisedButton label="Add post" fullWidth={true} backgroundColor="#00A0FF" labelColor="#ffffff"/>
      </div>  
    );
  }
}