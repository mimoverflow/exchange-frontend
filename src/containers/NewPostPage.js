import React from 'react'
import {NavigBar} from "./NavigBar"
import { RaisedButton } from 'material-ui';
import { PreviewListActions } from '../actions/PreviewListActions'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import TextField from 'material-ui/TextField';
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css"

class NewPostPage extends React.Component {
    constructor() {
        super()
        this.state = {
            title: '',
            content: '',
        }
    }
    handleTitleChange(ev) {
        this.setState({
            title: ev.target.value
        })
    }
    handleContentChange(ev) {
        this.setState({
            content: ev.target.value
        })
    }

    buttonHandler(ev) {
        ev.preventDefault();
        this.props.fetchNewPreview(this.state.title, this.state.content);
    }

    render() {

        return (
            <div>
            <NavigBar /> 
            <div className="container" style={{ backgroundColor: '#ffffff', color: '#000000'}}>
            	<div>
                    <TextField
                      hintText="Title"
                      fullWidth={true}
                      multiLine={true}
                      rows={1}
                      value={this.state.title}
                      onChange={this.handleTitleChange.bind(this)}
                    />
                    <TextField
                      hintText="Content"
                      fullWidth={true}
                      multiLine={true}
                      rows={1}
                      value={this.state.content}
                      onChange={this.handleContentChange.bind(this)}
                    /><br />
                  </div>
            	<br /><br />
            	<RaisedButton fullWidth={true} backgroundColor="#00A0FF" labelColor="#ffffff" 
                onClick={this.buttonHandler.bind(this)}>
                    Add post
                </RaisedButton>
            </div >
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchNewPreview: (title, content) => {
            dispatch(PreviewListActions.fetchNewPreview(title, content));
        }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NewPostPage));

