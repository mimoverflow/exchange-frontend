import React from 'react';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

export class SubjectList extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
      value: 1
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  handleChange = (event, index, value) => this.setState({value});
  render() {
    return (
      <div>
        <DropDownMenu value={this.state.value} onChange={this.handleChange}>
          <MenuItem value={1} primaryText="Calculus I" />
          <MenuItem value={2} primaryText="Calculus II" />
          <MenuItem value={3} primaryText="Discrete Mathematics" />
          <MenuItem value={4} primaryText="Functional Programming" />
          <MenuItem value={5} primaryText="Linear Algebra" />
        </DropDownMenu>
      </div>
    );
  }
}