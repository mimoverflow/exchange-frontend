import { ActionConstants } from "./ActionConstants"
import helpers from "../helpers"

export const ThreadActions = {
    fetchThread,
    fetchNewAnswer,
    fetchNewReAnswer,
    searchThreads
}

function fetchThread(id) {
    function fetchSuccessThread(pload) { return { type: ActionConstants.THREAD_FETCH_SUCCESS, payload: pload } }

    return (dispatch) => {
        let url = helpers.baseURL + "/api/threads/" + id
        fetch(url, { method: "GET", mode: 'cors' })
            .then((response) => response)
            .then((response) => response.json())
            .then((items) => {
                dispatch(fetchSuccessThread(items))
            })
            .catch((err) => { console.log(err) });
        helpers.history.push("/thread");
        localStorage.setItem("cur_thread_id",id);
    }
}

function fetchNewAnswer(thread_id, content) {
    function fetchSuccessAnswer(pload) { return { type: ActionConstants.ANSWER_FETCH_SUCCESS, payload: pload } }

    return (dispatch) => {
        console.log(thread_id, content)
        let url = helpers.baseURL + "/api/answers/"
        let fetchData = {
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify({
                "content": content,
                "thread": thread_id,
                "owner": helpers.owner_id
            }),
            headers: {
                'content-type': 'application/json'
            }
        }
        fetch(url, fetchData)
            .then((response) => response.json())
            .then((items) => {
                dispatch(fetchSuccessAnswer(items))
            })
            .catch((err) => console.log(err))
    }
}

function fetchNewReAnswer(answer_id, content) {
    return (dispatch) => {
        let url = helpers.baseURL + "/api/reanswers/"
        let fetchData = {
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify({
                "content": content,
                "answer": answer_id,
                "owner": helpers.owner_id
            }),
            headers: {
                'content-type': 'application/json'
            }
        }
        fetch(url, fetchData)
            .then((response) => response.json())
            .then((response) => {})
            .catch((err) => console.log(err))
    }
}
function searchThreads(keyword){
    function fetchSuccess(pload) { return {type: ActionConstants.SEARCH_FETCH_SUCCESS, payload: pload } }
    
    return (dispatch)=>{

        let url = helpers.baseURL + "/api/threads/filter/?title=" + keyword
        fetch(url, { method: "GET", mode: 'cors' })
            .then((response) => response)
            .then((response) => response.json())
            .then((items) => {
                dispatch(fetchSuccess(items))
            })
            .catch((err) => { console.log(err) });
        helpers.history.push("/");
    }
}


