import { ActionConstants } from "./ActionConstants"
import helpers from "../helpers"

export const PreviewListActions = {
    fetchPreviewList,
    fetchNextPreview,
    fetchNewPreview,
}

function fetchSuccess(pload) { return { type: ActionConstants.PREVIEWLIST_FETCH_SUCCESS, payload: pload } }

function fetchPreviewList() {
    //TODO add request in thread service
    //TODO add event objects
    return (dispatch) => {
        let url = helpers.baseURL + "/api/threads/"
        fetch(url, { method: "GET", mode: 'cors' })
            .then((response) => response.json())
            .then((items) => dispatch(fetchSuccess(items))) 
            .catch((err) => { console.log(err) })
    }
}

function fetchNextPreview(nextUrl) {
    return (dispatch) => {
        console.log("fetch next preview")
        fetch(nextUrl, { method: "GET", mode: 'cors' })
            .then((response) => response.json())
            .then((items) => dispatch(fetchSuccess(items)))
            .catch((err) => { console.log(err) });
    }
}

function fetchNewPreview(title, content) {
    return (dispatch) => {
        let url = helpers.baseURL + "/api/threads/"
        let fetchData = {
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify({
                "title": title,
                "content": content,
                "owner": helpers.owner_id
            }),
            headers: {
                'content-type': 'application/json'
            }
        }
        fetch(url, fetchData)
            .then((response) => response.json())
            .then((e)=>helpers.history.push('/'))
            .catch((err) => console.log(err))
    }
}