import { combineReducers } from 'redux';
import { currentThread } from './ThreadReducer'
import { previewList } from './PreviewListReducer'
const rootReducer = combineReducers({
    currentThread,
    previewList
});

export default rootReducer;