import { ActionConstants } from '../actions'

let initState={
    answers: []
}

export function currentThread(state = initState, action) {
	let next_state
    switch (action.type) {
        case ActionConstants.THREAD_FETCH_SUCCESS:
            next_state = { ...state, ...action.payload };
            return next_state
        case ActionConstants.ANSWER_FETCH_SUCCESS:
        	next_state = state
        	next_state.answers = [...next_state.answers, action.payload]
        	return next_state 
        default:
            return state
    }
}



