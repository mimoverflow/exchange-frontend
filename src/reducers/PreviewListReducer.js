import { ActionConstants } from '../actions'

let initial = { count: 0, next: null, previous: null, results: [] }

export function previewList(state = initial, action) {
    let next_state
    switch (action.type) {
        case ActionConstants.PREVIEWLIST_FETCH_SUCCESS:
            next_state = action.payload
            if (action.payload.previous != null)
                next_state.results = [...state.results, ...action.payload.results]
            return next_state;
        case ActionConstants.PREVIEWLIST_FETCH_FAILURE:
            return { ...state };
        case ActionConstants.SEARCH_FETCH_SUCCESS:
            next_state = state
            next_state.results = [...action.payload]
            return next_state
        default:
            return state
    }
}
